<?php
/**
 * @file
 * Contains Drupal\accesibility\Form\MessagesForm.
 */
namespace Drupal\accesibility\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class MessagesForm extends ConfigFormBase {
    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            'accesibility.adminsettings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'accesibility_form';
    }
    /**
     * {@inheritdoc}
     */

    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config('accesibility.adminsettings');

        $form['accesibility_message1'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Night mode'),
            '#description' => $this->t(''),
            '#default_value' => $config->get('accesibility_message1'),
        ];

        $form['accesibility_message2'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Simple navigation'),
            '#description' => $this->t(''),
            '#default_value' => $config->get('accesibility_message2'),
        ];



        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        parent::submitForm($form, $form_state);

        $test = \Drupal::service('extension.list.module')->getPath('accesibility');

        $this->config('accesibility.adminsettings')
            ->set('accesibility_message1', $form_state->getValue('accesibility_message1'))
            ->save();

        $file1=$test.'/css/dark.css';

        file_put_contents($file1, $form_state->getValue('accesibility_message1'));

        $this->config('accesibility.adminsettings')
            ->set('accesibility_message2', $form_state->getValue('accesibility_message2'))
            ->save();

        $file2=$test.'/css/simple.css';

        file_put_contents($file2, $form_state->getValue('accesibility_message2'));
    }

}
